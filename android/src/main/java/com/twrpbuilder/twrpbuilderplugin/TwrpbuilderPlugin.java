package com.twrpbuilder.twrpbuilderplugin;

import com.stericson.RootTools.RootTools;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** TwrpbuilderPlugin */
public class TwrpbuilderPlugin implements MethodCallHandler {

  //public static boolean ROOT_GRANTED;
  /** Plugin registration. */
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "twrpbuilder_plugin");
    channel.setMethodCallHandler(new TwrpbuilderPlugin());
  }

  @Override
  public void onMethodCall(MethodCall call, Result result) {
    switch (call.method) {
      case "isAccessGiven":
        result.success(isAccessGiven());
        break;
      default:
        result.notImplemented();
        break;
    }
  }

  private boolean isAccessGiven(){
    return RootTools.isAccessGiven();
  }
}
