# TWRP Builder Plugin

A new Flutter plugin only for Android.
This plugin is for [TWRPBuilder](https://github.com/TwrpBuilder/TwrpBuilder/tree/flutter) app only.
Although you can use this plugin to get root access in your app.
## TO-DO
* ~~Add method to get root access~~
* Add method to execute Shell commands
## Note

This plugin is WIP. 

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).

For help on editing plugin code, view the [documentation](https://flutter.io/platform-plugins/#edit-code).