import 'dart:async';

import 'package:flutter/services.dart';

class TwrpbuilderPlugin {
  static const MethodChannel _channel =
      const MethodChannel('twrpbuilder_plugin');

  static Future<bool> get rootAccess async {
    final bool access = await _channel.invokeMethod('isAccessGiven');
    return access;
  }
}
